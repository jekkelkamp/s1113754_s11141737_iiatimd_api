<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','apiController@APIhome');
Route::get('/items','apiController@getItems');
Route::get('/items/weapons','apiController@getWeapons');
Route::get('/items/armor','apiController@getArmor');
Route::get('/items/consumables','apiController@getConsumables');
Route::get('/items/focus','apiController@getFocus');
Route::get('/items/potions','apiController@getPotions');
Route::get('/items/tools','apiController@getTools');
Route::get('/item/{itemId}','apiController@getItemById');

    // return view('welcome');
    //Route::get('achievement/edit','AchievementController@getAchievementToEdit')->name('achievement/edit');
