<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('cost');
            $table->string('currency');
            $table->string('type')->references(['name','name','name','name','name','name','name'])->on(['armor','weapons','consumables','tools','potions','focus','gear']);
            $table->float('weight');
            $table->string('image_src_white')->nullable();
            $table->string('image_src_black')->nullable();
        });
    }

    /**typetype
     * Reverse the migrations.name
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
