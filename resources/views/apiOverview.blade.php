<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>D&D item API</title>

        <!-- Fonts -->
         <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <style>

      *{
        font-family: 'Roboto', sans-serif;
      }

      body{
        background-color: #282a36;
        width: 101rem;
        height: 55rem;
        margin: auto;

      }
      h1 {
        margin: 3rem 0 1rem 1rem;
        font-weight: bold;
        color: white;
        user-select: none;

      }
      main{
        border-radius: .5rem;
        background-color: #44475a;
        width: 100%;
        height: 85%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        color: white;
        overflow-y: scroll;
        overflow-x: hidden;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
        user-select: none;
      }
      .tabs {
        /* position: absolute; */
        margin-left: 1rem;
        display: grid;

        grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
        grid-column-gap: .2rem;
        width: 20%;
        user-select: none;
      }

      .tab {
        text-align: center;
        border-top-left-radius: .5rem;
        border-top-right-radius: .5rem;
        width: 6rem;
        padding: 1rem;
        color: white;
        background-color: #6272a4;
        font-weight: bold;
        transition: all .5s ease;
        user-select: none;
      }
      .tab:hover {
        opacity: 0.5;
        cursor: pointer;
      }

      .active {
        background-color: #ffb86c;
      }

            /* width */
      ::-webkit-scrollbar {
        width: 10px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
        background: #f1f1f1;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: #888;
      }

      /* Handle on hover */
      ::-webkit-scrollbar-thumb:hover {
        background: #555;
        cursor: pointer;
      }

      .card {
        width: 12rem;
        border: 2px solid #6272a4;
        text-align: center;
        border-radius: .5rem;
        margin: 1rem;
        height: 9rem;

      }

      .card__header {
        /* padding: .5rem; */
        background-color: #6272a4;
        width: 100%;
        height: 72%;
      }

      .card__info{
        margin-top: 1rem;
        margin-bottom: 1rem;
      }

      .search{
        background-color:inherit;
        margin-left: 28rem;
        padding: .5em;
        margin-bottom: 5px;
        border: none;
        border-bottom: 5px solid #ffb86c;
        color: white;
      }

    </style>
    </head>

    <body>
      <h1>D&D item API</h1>
          <div class="tabs">
            <div class="tab active">Items</div>
            <div class="tab">Weapons</div>
            <div class="tab">Armor</div>
            <div class="tab">Consumables</div>
            <div class="tab">Focus</div>
            <div class="tab">Potions</div>
            <div class="tab">Tools</div>
            <input id="js--searchBar" onkeyup="searchFunction()" type="text" placeholder="Search for an item" class="search"></input>
          </div>
      <main>
        @foreach($items as $item)
        <div class="card">
          <div class="card__header">

            <img src="{{url($item->image_src_white)}}" alt="Adventuring gear" />


          </div>
          <div class="card__info">
            {{$item->name}}
          </div>
        </div>
        @endforeach
      </main>

      <script>
      let search = document.getElementById('js--searchBar');
      search.value = "";
      searchFunction = () => {
        let search = document.getElementById('js--searchBar');
        let cardinfo = document.getElementsByClassName('card__info');
        let cards = document.getElementsByClassName('card');
        let filter = search.value.toUpperCase();
        for(i = 0; i < cards.length; i++){
          let txtValue = cardinfo[i].textContent || cardinfo[i].innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
              cards[i].style.display = "";
          } else {
              cards[i].style.display = "none";
          }
          }
      }
      </script>
    </body>
</html>
