<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Armor;
use App\Consumable;
use App\Focus;
use App\Potion;
use App\Tool;
use App\Weapon;
use App\Gear;

class apiController extends Controller
{
    public function APIhome() {
      return view('apiOverview')
        ->with('items',Item::all())
        ->with('weapons',Weapon::all())
        ->with('armor',Armor::all())
        ->with('consumables',Consumable::all())
        ->with('focus',Focus::all())
        ->with('potions',Potion::all())
        ->with('tools',Tool::all())
        ->with('gear',Gear::all())
        ;
    }
    public function getItems() {
      // return Item::all();
      $items = Item::all();
      $weapons = Weapon::all();
      foreach($items as $item) {
        foreach($weapons as $weapon) {
          if($item->type == $weapon->name) {
            $item->relationInfo = $weapon;
          }
        }
      }
      return $items;
    }

    public function getItemById($itemId){
      return Item::where('id','=',$itemId)->first();

    }

    public function getWeapons() {
      return Weapon::all();
    }

    public function getArmor() {
      return Armor::all();
    }

    public function getConsumables() {
      return Consumable::all();
    }

    public function getFocus() {
      return Focus::all();
    }

    public function getPotions() {
      return Potion::all();
    }

    public function getTools() {
      return Tool::all();
    }
}
